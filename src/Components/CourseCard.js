import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState} from 'react';
export default function CourseCard({courseProp}){
	// console.log({courseProp});
	// console.log(typeof {courseProp});

//Deconstruct the course properties into their own variables(destructuring)
const {name, description, price} = courseProp;

//Use the state hook for this comment to be able to store its state
//States are use dto keep track of information related to individual component
//Syntax
 	//const [getter, setter] = useState(initialGetterValue)
 	//getter = stored initial (default value)
 	//setter = uppdated value

const [count, setCount] = useState(10)
const [add, setAdd] = useState(0)

function enroll() {
	setCount(count - 1);
	let seats = document.querySelector("#seats").value;
	seats = count;
	document.querySelector('#seats').innerHTML = seats;
	if (count === 0){
		alert("No more seats available")
	}

}

function reEnroll() {
	setAdd(add + 1);
	let enrollees = document.querySelector("#enrollees").value;
	enrollees = add;
	document.querySelector('#enrollees').innerHTML = enrollees;
	if (add === 10){
		alert("Full Capacity")
	}
}

function both(){
	enroll();
	reEnroll();
}




	return(
		<Card>
		  <Card.Body>
		    <Card.Title><h2>{name}</h2></Card.Title>
		      <Card.Subtitle>Description:</Card.Subtitle>
		      <Card.Text>{description}</Card.Text>
		      <Card.Subtitle>Price:</Card.Subtitle>
		      <Card.Text>{price}</Card.Text>
		      <Card.Text>Enrollees:</Card.Text>
		      <Card.Text id="enrollees">0</Card.Text>
		      <Card.Text>Seats:</Card.Text>
		      <Card.Text id="seats">10</Card.Text>
		    <Button variant="primary" onClick={both}>Enroll</Button>
		  </Card.Body>
		</Card>
		)
}

// Check i fthe CourseCard Component is getting the correct prop ttypes
// Proptypes are used fro validating information passed to a component and is a tool 
// normally used to help developers ensure the correct information is passed from one
//component to the next

CourseCard.propTypes = {
	//The 'shape' method is used to check if a prop object conforms to a specific 'shape';
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}