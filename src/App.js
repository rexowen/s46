import './App.css';
import {Fragment} from 'react';
import AppNavbar from './Components/AppNavbar';

//pages
import Home from './pages/Home';
import Courses from './pages/Courses';

//Bootstrap
import {Container} from 'react-bootstrap';

function App() {
  return (
    <Fragment>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses />
      </Container>
    </Fragment>
  );
}

export default App;

/*

With the React Fragment Component, we can g roup multiple components and avoid adding extra code



JSX Syntax
JSX or Javascript XML is an extension to t he syntax of JS. It allows us to write
HTML-like syntax within our React js projects and it cinldues JS features as well
Install the Js(Babel) linting for code readability
Ctrl + Shift + P
In the input field enter Package Control
Install Babel

*/