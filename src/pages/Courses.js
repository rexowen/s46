import courseData from '../data/courseData';
import CourseCard from '../Components/CourseCard';
import { Fragment } from 'react';

export default function Courses(){
	console.log(courseData);
	console.log(courseData[0]);

//for us to be able to display all the courses from the data file, we are going to use the map()
//The map() method loops through the individual course objeccts in our array and returns a component for each course
//Multiple comments createed through the map method must have a UNIQUE KEY that will help React js identify which /elements have been changed, added

const courses = courseData.map(course => {
	return(
		<CourseCard key={course.id} courseProp={course}/>

		)
})


	return(
		<Fragment>
		<h1>Courses</h1>
		{courses}
		</Fragment>
	)
}